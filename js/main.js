$(document).ready(function() {
	// Start Data Request
	$('#start').on("click", function(){
	$('#start').hide();	
	$.ajax({
			url:"data/web-assistent-c4rl.json",
			method:"GET",
			chache:true,
			dataType: "json",
			crossDomain: true

			// Request succeful
			// Work with data
		}).done(function(data){
			// Design chat style baloons
			var c4rl = '<div class="row"><div class="col-lg-6 col-11 mb-1"><div class="baloonC4rl d-inline-block p-2 rounded">content</div></div></div>';
			var user = '<div class="row"><div class="col-lg-6 col-11 offset-lg-6 offset-1 mb-1"><div class="baloonIntent float-right d-inline-block p-2 rounded">content</div></div></div>';
			var time = 100;
			// c4rl
			$.each(data.intro.message, function(i,val){
				var c4rlSpeak = function(text){
					return c4rl.replace(
							'content', text
						)
				};
				//Convert string length to period of time
				time += val.length*50;

				// Delay
				setTimeout(
					function(){
						let element = $('#chat').append(c4rlSpeak(val));
						element[0].scrollIntoView(false);
					},
					time
					);
				console.log(time);
				
			});
			// Render intent for the user
			$.each(data.index.intent, function(i, val){
				let intentButton = function(text) {
					let str = '<a class="btn btn-c4rl mr-1 mb-2 intent text-white">content</a>';

					return str.replace(
							'content', text
						)
				};

				var time = 7000;

				// Delay
				setTimeout(
					function(){
					$('#intentsList').append(intentButton(val.value));
				},
					time
					);
				time += 200;
			});
		
			// C4rl
			$('#intentsList').on('click', 'a',  function(){
					
					let userIntent = function(text) {
						return user.replace(
								'content', text
							)
					}
					if ($(this).text() === "Mario's Story") {
						
						// User
						let element = $('#chat').append(userIntent("I am interested to know more about Mario's story"));
						element[0].scrollIntoView(false);
						$('#intentsList').addClass('d-none');

						var time=0;
						
						// C4rl
						$.each(data.index.intent[0].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};
							
							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							console.log(time);
							
						});
						// Render intent for the user

						$.each(data.index.intent[0].intent, function(i, val){
							let intentButton = function(text) {
								let str = '<a class="btn btn-c4rl intentButton mr-1 mb-2 intent text-white">content</a>';

								return str.replace(
										'content', text
									)
							};

							var time = val.length*50;

							// Delay
							setTimeout(
								function(){
									$('#subIntentsList').append(intentButton(val.value));

							},
								time
								);
							time += 200;
						});

					}if ($(this).text() === "Contacts") {
						let element = $('#chat').append(userIntent("I'd like to contact Mario"));
						element[0].scrollIntoView(false);	

						var time =0;
						$.each(data.index.intent[1].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};

							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							
						});

						
					}if ($(this).text() === "Skills") {
						let element = $('#chat').append(userIntent("Tell me what Mario's skills are"));
						element[0].scrollIntoView(false);
						var time=0;
						$.each(data.index.intent[2].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};

							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							
						});

						
					}if ($(this).text() === "Nothing else") {
						let element = $('#chat').append(userIntent("I don't need anything, thanks!"));
						element[0].scrollIntoView(false);	
						var time=0;
						$.each(data.index.intent[3].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};

							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							
						});

						
					}
				});

				// C4rl response
				$('#subIntentsList').on('click', 'a',  function(){
					
					var time = 500;
					let userIntent = function(text) {
						return user.replace(
								'content', text
							)
					}
					if ($(this).text() === "Yes (work experience)") {
						let element = $('#chat').append(userIntent("It would be nice to know Mario's work experience"));
						element[0].scrollIntoView(false);
						console.log(element[0]);
						var time=0;
						// C4rl
						$.each(data.index.intent[0].intent[0].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};
							time += val.length*50;
							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							
						});

					}if ($(this).text() === "Yes (job availability)") {
						let element = $('#chat').append(userIntent("Is Mario available for other projects and collaborations?"));
						element[0].scrollIntoView(false);
						var time=0;
						$.each(data.index.intent[0].intent[1].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};

							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							
						});

						
					}if ($(this).text() === "No, thanks") {
						$('#chat').append(userIntent("I appreciate it, but at the moment I am not interested in more information."));
						$('#subIntentsList').empty();
						var time=0;
						$.each(data.index.intent[0].intent[2].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};
							
							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);

							
						});
						$('#intentsList').removeClass('d-none');
						
					}if ($(this).text() === "Nothing") {
						$('#chat').append(userIntent("I don't need anything, thanks!"));
						var time=0;
						$.each(data.index.intent[3].message, function(i,val){
							var loader = $('<div class="spinner-grow" role="status"><span class="sr-only">Loading...</span></div>');
							var c4rlSpeak = function(text){
								return c4rl.replace(
										'content', text
									)
							};

							time += val.length*50;

							// Delay
							setTimeout(
								function(){
									let element = $('#chat').append(c4rlSpeak(val));
									element[0].scrollIntoView(false);
								},
								time
								);
							
						});

						
					}
				});
		}).fail(function(msg){
			alert('An error occured');
		}
		
		).always();
	});

	
});